//Forma #1
/*/Modulo
var app = angular.module("MyFirstApp",["ngResource"]);




app.controller("FirstController",function($scope){

	$scope.nombre = "Mario Galindo"; 
	//Scope lo que hace es que enlaza la informacion
	//del controlador con la vista

});
*/


//Otra Forma funciona igual que la de arriba
angular.module("MyFirstApp",[])
.controller("FirstController",["$scope",function(m){

	m.nombre = "Mario Galindo "; 
	//Scope lo que hace es que enlaza la informacion
	//del controlador con la vista
	m.nuevoComentario =  {}


	m.comentarios = [

		{
			comentario:"Buen Tutorial",
			username:"Mario Galindo"
		},

		{
			comentario:"Malisimo el Tutorial",
			username:"Otro Usuario"

		}	

	];



	m.agregarcomentario = function(){
		

		m.comentarios.push(m.nuevoComentario);
		m.nuevoComentario = {};  
		
	};

}]);




//Modelo Vista vista modelo MVVM - aun no lo vemos
//Las vistas pues son el HTML

