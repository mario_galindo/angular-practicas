angular.module('TodoDoList', ["LocalStorageModule"])

.service('ToDoService',function(localStorageService){

	

	this.key = "clave";	

			if(localStorageService.get(this.key)){

				this.activities = localStorageService.get(this.key);

			}else{

				this.activities = [];
			}

			this.add = function(newActv){
				this.activities.push(newActv);
				this.updateLocalStorage();
			};

			this.updateLocalStorage = function(){
				localStorageService.set(this.key,this.activities);
			}


			this.clean = function(){
				this.activities = [];
				this.updateLocalStorage();
				return this.getAll();
			}


			this.getAll = function(){
				return this.activities;
			}


			this.removeItem = function(item){
				this.activities = this.activities.filter(function(activty){
					return activty !== item;
				});
				
				this.updateLocalStorage();
				return this.getAll();
				/*
					[{},{}] -> todoservce.activities

				*/
			}

	
})

	.controller('listaController',function($scope,ToDoService){

		$scope.todo = ToDoService.getAll();
		$scope.nuevaActividad = {};
		$scope.agregarActividad = function(){

			
			ToDoService.add($scope.nuevaActividad);
			$scope.nuevaActividad = {};
		}

		$scope.removeItem = function(item){
			$scope.todo = ToDoService.removeItem(item);
		}


		$scope.clean = function(){
			$scope.todo = ToDoService.clean();
		} 
	});