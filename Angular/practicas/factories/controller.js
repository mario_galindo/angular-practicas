angular.module('TodoDoList', ["LocalStorageModule"])

.factory('ToDoService',function(localStorageService){

	var ToDoService = {};

	ToDoService.key = "clave";	

			if(localStorageService.get(ToDoService.key)){

				ToDoService.activities = localStorageService.get(ToDoService.key);

			}else{

				ToDoService.activities = [];
			}

			ToDoService.add = function(newActv){
				ToDoService.activities.push(newActv);
				ToDoService.updateLocalStorage();
			};

			ToDoService.updateLocalStorage = function(){
				localStorageService.set(ToDoService.key,ToDoService.activities);
			}


			ToDoService.clean = function(){
				ToDoService.activities = [];
				ToDoService.updateLocalStorage();
				return ToDoService.getAll();
			}


			ToDoService.getAll = function(){
				return ToDoService.activities;
			}


			ToDoService.removeItem = function(item){
				ToDoService.activities = ToDoService.activities.filter(function(activty){
					return activty !== item;
				});
				
				ToDoService.updateLocalStorage();
				return ToDoService.getAll();
				/*
					[{},{}] -> todoservce.activities

				*/
			}

	return ToDoService;	
})

	.controller('listaController',function($scope,ToDoService){

		$scope.todo = ToDoService.getAll();
		$scope.nuevaActividad = {};
		$scope.agregarActividad = function(){

			
			ToDoService.add($scope.nuevaActividad);
			$scope.nuevaActividad = {};
		}

		$scope.removeItem = function(item){
			$scope.todo = ToDoService.removeItem(item);
		}


		$scope.clean = function(){
			$scope.todo = ToDoService.clean();
		} 
	});