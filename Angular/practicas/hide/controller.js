var app = angular.module("PrimeraApp",[])

app.controller("PrimerControlador",function($scope,$http){


	//creamo un arreglo de POST
	$scope.posts = [];
	$scope.loading = true;

	
	//Usamos la API de jsonplaceholder
	//Peticion AJAX al servidor para traer los datos
	$http.get("http://jsonplaceholder.typicode.com/posts")
		.success(function(data){ //el parametro que recibe es lo que retorna el servidor

			console.log(data); //retorna un array que tiene todos los posts de la API
			$scope.posts = data;
			$scope.loading = false; 
		})
		.error(function(error){ //el parmetro que recibe es lo que retorna el servidor

			$scope.loading = false;
		});


		

});