var app = angular.module("PrimeraApp",[])

app.controller("PrimerControlador",function($scope,$http){


	//creamo un arreglo de POST
	$scope.posts = [];
	$scope.newPost = {}; 

	
	//Usamos la API de jsonplaceholder
	//Peticion AJAX al servidor para traer los datos
	$http.get("http://jsonplaceholder.typicode.com/posts")
		.success(function(data){ //el parametro que recibe es lo que retorna el servidor

			console.log(data); //retorna un array que tiene todos los POST de la API
			$scope.posts = data; 
		})
		.error(function(error){ //el parmetro que recibe es lo que retorna el servidor


		});


		


		//Ya no vamos a leer datos si no que vamos a crear entonces utilizamos el metodo POST
		$scope.addPost = function(){

			$http.post("http://jsonplaceholder.typicode.com/posts",{

				title: $scope.newPost.title,
				body:  $scope.newPost.body,
				userId: 1
			})

			.success(function(data,status,headers,config){

				console.log(data)
				$scope.posts.push($scope.newPost);
				$scope.newPost = {}

					
			})
			.error(function(error,status,headers,config){
					console.log(error)
			})
					
				
		} 


});