//Data Binding
//Es la sincronizacoion entre el modelo y la vista a traves del objeto $scope
angular.module('customDirective', [])

.controller('AppCtrl',function ($scope,$http) {
	
	$http.get("https://api.github.com/users/marioCoder94/repos")//retorna un promise
		.success(function(data){
			$scope.repos = data;
		})
		.error(function(err){
			console.log(err);
		});
	

});
