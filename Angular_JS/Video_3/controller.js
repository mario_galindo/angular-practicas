
angular.module('PrimeraAplicacion', []) //Esta instruccion retorna una referencia a la aplicacion


//Creacion del Controlador con la directiva $scope que es la que me enlaza la vista con el controlador 
//y me hace el binding de la informacion es como un VM (vista modelo)

.controller('PrimerControlador', ['$scope', function ($scope) {

	$scope.nombre = "Mario Galindo";	

}]);