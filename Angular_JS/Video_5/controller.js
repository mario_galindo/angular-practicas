//Data Binding
//Es la sincronizacoion entre el modelo y la vista a traves del objeto $scope
angular.module('DataBinding_App', [])

.controller('Ejemplo_databinding', ['$scope', function ($scope) {
	
	
	//Atributos de mi controlador

	$scope.nombre = "Mario Galindo";
	$scope.nuevoComentario = {};

	//Creamos un arreglo para los comentarios
	$scope.comentarios = [

		{
			comentario:"Buen Tutorial",
			username: "marioga94"
		},
		{
			comentario:"Malisimo Tutorial",
			username: "dani99"
		}

	];

	
	//Metodo para nuestro controlador
	$scope.agregarComentario = function(){

		$scope.comentarios.push($scope.nuevoComentario);
		$scope.nuevoComentario = {}; // Reiniciamos la variable
	}

}]);
